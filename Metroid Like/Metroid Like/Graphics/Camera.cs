﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Metroid_Like.Graphics
{
    // a class that is used for a camera, the camera stays in the provided boundrys
    public class Camera
    {
        //the boundrys for the camera
        private Rectangle cameraBounds;
        //the camera position
        private Vector2 cameraPosition;
        // the camera instance
        private static Camera instance = null;

        //returns or sets the bound for the camera
        public Rectangle CameraBounds
        {
            get
            {
                return cameraBounds;
            }

            set
            {
                cameraBounds = value;
            }
        }

        //returns or sets the position for the camera
        public Vector2 CameraPosition
        {
            get
            {
                return cameraPosition;
            }
            set
            {
                cameraPosition = value;
            }
        }

        //returns or sets the X coord for the camera
        public float X
        {
            get
            {
                return cameraPosition.X;
            }
            set
            {
                cameraPosition.X = value;
            }
        }

        //returns or sets the Y coord for the camera
        public int UpperBound
        {
            get
            {
                return cameraBounds.Height;
            }
            set
            {
                cameraBounds.Height = value;
            }
        }

        //returns or sets the lowerbound for the camera
        public int LowerBound
        {
            get
            {
                return cameraBounds.Width;
            }
            set
            {
                cameraBounds.Width = value;
            }
        }
        

        //returns or sets the left bound for the camera
        public int LeftBound
        {
            get
            {
                return cameraBounds.X;
            }
            set
            {
                cameraBounds.X = value;
            }
        }

        //returns or sets the right bound for the camera
        public int RightBound1
        {
            get
            {
                return cameraBounds.Y;
            }
            set
            {
                cameraBounds.Y = value;
            }
        }

        //defualt constructor-- creates a new isntance of Camera
        private Camera()
        {
            cameraBounds = Rectangle.Empty;
            cameraPosition = Vector2.Zero;

        }

        //returns the camera instance if one is not already created a new instance will be created
        public Camera GetInstance()
        {
            return instance == null ?instance = new Camera() : instance;
        }


    }
}
