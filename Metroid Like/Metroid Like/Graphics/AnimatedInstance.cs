﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
namespace Metroid_Like.Graphics
{
    //this class is used for animating a sprite
    class AnimatedInstance : DrawnInstance
    {
        //the number of frames for the sprite
        private int numberOfFrames;
        //the current frame the instance is on
        private int currentFrame;

        //returns or sets the number of frames
        public int NumberOfFrames
        {
            get
            {
                return numberOfFrames;
            }
            set
            {
                numberOfFrames = value;
            }
        }

        //returns or sets the current frame
        public int CurrentFrame
        {
            get
            {
                return currentFrame;
            }
            set
            {
                currentFrame = value;
            }
        }
        
        //overloaded constructor-- creates a new instance of AnimatedInstance with overloaded parameters
        //texture- the texture for this instance
        //numberofFrames- the number of frames for this instance
        //x- the x cood for this instance
        //y- the y coord ofr this instance
        //width- the width for this instance
        //height- the height for this instance
        //rotation- the rotation for this instance
        public AnimatedInstance(Texture2D texture = null, int numberOfFrames = 0, float x = 0, float y = 0,float width = 0,float height = 0
            ,float rotation = 0): base(texture,x,y,width,height,rotation)
        {
            //set the number of frames
            this.numberOfFrames = numberOfFrames;
        }

        //increases the frame by one
        public void Animate()
        {
            
            currentFrame++;
            //advance the frame
            currentFrame %= numberOfFrames;
        }

        //draws the animated instance on the current frame
        //sprBatch- the sprite batch to use for drawing
        public override void Draw(SpriteBatch sprBatch)
        {
            //draw the current frame
            sprBatch.Draw(Texture, new Rectangle(Positon.ToPoint(), Size.ToPoint()), new Rectangle((int)(currentFrame * ((float)Texture.Width / numberOfFrames)),
                0, (int)((float)Texture.Width / numberOfFrames), Texture.Height), Color.White);
        }
    }
}
