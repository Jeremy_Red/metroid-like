﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Metroid_Like
{
    //this is an abstract class intended to be overridded by an object that is drawn
    public class DrawnInstance
    {
        //the position of the instance
        private Vector2 position;
        //the size of the instance
        private Vector2 size;
        //the rotation of the instance
        private float rotation;
        //the texture of the instance
        private Texture2D texture;

        //returns or sets the texture of this instance
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }

        //returns or sets the size of this instance
        public Vector2 Size
        {
            get
            {
                return size;
            }

            set
            {
                size = value;
            }
        }

        //returns or sets the width of this instace
        public float Width
        {
            get
            {
                return size.X;
            }
            set
            {
                size.X = value;
            }
        }

        //returns or sets the height of this instance
        public float Height
        {
            get
            {
                return size.Y;
            }
            set
            {
                size.Y = value;
            }
        }

        //returns or sets the positon of this instance
        public Vector2 Positon
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        //returns or sets the x coord of the instance
        public float X
        {
            get
            {
                return position.X;
            }
            set
            {
                position.X = value;
            }
        }

        //returns or sets the y coord of the instance
        public float Y
        {
            get
            {
                return position.Y;
            }

            set
            {
                position.Y = value;
            }
        }

        //returns or sets the rotation of the instance
        public float Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
            }
        }

        //overloaded constructor-- creates a new instance of DrawnInstance with overloaded parameters
        //texture- the texture for this instance
        //x- the x coord for this instance
        //y- the y coord for this instance
        //width- the width of this instance
        //height- the height of this instance
        //rotation- the rotation of this instance
        public DrawnInstance(Texture2D texture = null,float x = 0,float y = 0,float width = 0,float height = 0,float rotation = 0)
        {
            //set the texture
            this.texture = texture;
            //set the position
            this.position = new Vector2(x, y);
            //set the size
            this.size = new Vector2(width, height);
            //set the rotation
            this.rotation = rotation;
        }

        //draws the instance on a spritebatch
        //sprBatch- the sprite batch to use to draw the instance
        public virtual void Draw(SpriteBatch sprBatch)
        {
            sprBatch.Draw(texture, new Rectangle(position.ToPoint(), size.ToPoint()), Color.White);
        }
    }
}
