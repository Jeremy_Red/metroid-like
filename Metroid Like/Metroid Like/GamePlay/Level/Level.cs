﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Drawing;
using Microsoft.Xna.Framework;

namespace Metroid_Like.GamePlay.Level
{
    //this class will be used for loading a single level
    class Level
    {
        Vector2 playerSpawn;
        private class TileTemplate
        {
            public Texture2D texture;
            public string tag;
        }

        private class InteractiveObject
        {
            public Texture2D texture;
            public string tag;
        }

        private InteractiveObject[] interactiveObjectTemplates = new InteractiveObject[256];

        private TileTemplate[] tileTemplates = new TileTemplate[256];

        private LinkedList<DrawnInstance> objectsToBeDrawn;

        private int tileWidth;

        private int tileHeight;

        private int[][] tiles;

        private int[][] interactiveObjects;

        public Level(ContentManager contentManager,string levelFile)
        {
            //init the tiles
            for(int i =0;i < tileTemplates.Length;i++)
            {
                tileTemplates[i] = null;
            }

            //init the interactive object templates
            for(int i=0;i < interactiveObjectTemplates.Length;i++)
            {
                interactiveObjectTemplates[i] = null;
            }

            //init the objects to be drawn
            objectsToBeDrawn = new LinkedList<DrawnInstance>();

            //call readLevels
            readLevel(contentManager, levelFile);
        }

        protected virtual void parseCustomData(string[] args)
        {
            return;
        }

        private void readLevel(ContentManager contentManager,string levelFile)
        {
            //start reading the level
            string[] lines = System.IO.File.ReadAllLines(levelFile);
            string[] args;
            args = lines[0].Split(' ');
            string levelData = "";
            int index = 0;
            do
            {
                switch(args[0].ToUpper())
                {
                    case "LEVEL_DATA":
                        levelData = args[1];
                        break;
                    case "TILE_DEFINITION":
                        //read the tile index
                        int tileIndex = int.Parse(args[1]);
                        index++;
                        //add the tile to the defined tile templates
                        tileTemplates[tileIndex] = readTile(contentManager,lines,ref index);
                        break;
                    case "INCLUDE":
                        //read the included file recursivly
                        readLevel(contentManager, args[1]);
                        break;
                    case "INTERACTIVE_OBJECT":
                        //read the interactive object in
                        int interactiveObjectIndex = int.Parse(args[1]);
                        index++;
                        //add the interactive object to the defined interactiveObjects
                        interactiveObjectTemplates[interactiveObjectIndex] = readInteractiveObject(contentManager, lines, ref index);
                        break;
                    case "TILE_WIDTH":
                        //read the tile width
                        tileWidth = int.Parse(args[1]);
                        break;
                    case "TILE_HEIGHT":
                        //read the tile height
                        tileHeight = int.Parse(args[1]);
                        break;
                    default:
                        //read the custom data
                        parseCustomData(args);
                        break;


                }
                index++;
            }while ((args = lines[index].Split(' '))[0].ToUpper() != "END");

            //read the tiles
            readLevelDataFromTexture(contentManager, levelData);
        }

        private TileTemplate readTile(ContentManager contentManager,string[] lines,ref int index)
        {
            TileTemplate newTile = new TileTemplate();
            string[] args = lines[index].Split(' ');
            newTile.tag = "NONE";
            do
            {
                switch (args[0].ToUpper())
                {
                    case "TEXTURE":
                        newTile.texture = contentManager.Load<Texture2D>(args[1]);
                        break;
                    case "TAG":
                        newTile.tag = "";
                        for(int i =1;i < args.Length;i++)
                        {
                            newTile.tag += args[i].ToUpper();
                        }
                        break;
                }
                index++;
            } while ((args = lines[index].Split(' '))[0].ToUpper() != "END");
            return newTile;
        }

        private InteractiveObject readInteractiveObject(ContentManager contentManager, string[] lines, ref int index)
        {
            InteractiveObject newTile = new InteractiveObject();
            string[] args = lines[index].Split(' ');
            newTile.tag = "NONE";
            do
            {
                switch (args[0].ToUpper())
                {
                    case "TEXTURE":
                        newTile.texture = contentManager.Load<Texture2D>(args[1]);
                        break;
                    case "TAG":
                        newTile.tag = "";
                        for (int i = 1; i < args.Length; i++)
                        {
                            newTile.tag += args[i].ToUpper();
                        }
                        break;
                }
                index++;
            } while ((args = lines[index].Split(' '))[0].ToUpper() != "END");
            return newTile;
        }

        public virtual void Draw(SpriteBatch sprBatch,int camX,int camY)
        {
            //draw the tiles
            foreach(DrawnInstance instance in objectsToBeDrawn)
            {
                int x = (int)instance.X;
                int y = (int)instance.Y;
                int width = tileWidth;
                int height = tileHeight;
                sprBatch.Draw(instance.Texture, new Microsoft.Xna.Framework.Rectangle(x-camX,y-camY,width,height), Microsoft.Xna.Framework.Color.White);
            }
        }

        private void readLevelDataFromTexture(ContentManager contentManager,string fileName)
        {
            Bitmap bitmap = new Bitmap(fileName);
            //init the tiles
            tiles = new int[bitmap.Width][];
            for (int i = 0; i < bitmap.Width; i++)
            {
                tiles[i] = new int[bitmap.Height];
            }

            //init the interactiveObjects
            interactiveObjects = new int[bitmap.Width][];
            for (int i = 0; i < bitmap.Width; i++)
            {
                interactiveObjects[i] = new int[bitmap.Height];
            }

            //read all of the tiles
            for (int x = 0;x < bitmap.Width;x++)
            {
                for(int y = 0;y < bitmap.Height;y++)
                {
                    int RorGorB = bitmap.GetPixel(x, y).R > 0 ? 0 : bitmap.GetPixel(x, y).G > 0 ? 1 : bitmap.GetPixel(x,y).B > 0 ? 2:-1;
                    byte color;
                    switch(RorGorB)
                    {
                        case 0:
                            //its a enemy so ignore this for now
                            break;
                        case 1:
                            //its an interactive object so record it in the list of interactive objects
                            color = bitmap.GetPixel(x, y).G;
                            if (interactiveObjectTemplates[color] != null)
                                interactiveObjects[x][y] = color;
                            else
                                interactiveObjects[x][y] = -1;
                            objectsToBeDrawn.AddLast(new DrawnInstance(tileTemplates[color].texture, tileWidth * x, tileHeight * y, tileWidth, tileHeight));
                            break;
                        case 2:
                            //its a tile so record it in the list of tiles if it exists
                             color = bitmap.GetPixel(x, y).B;
                            if (tileTemplates[color] != null)
                                tiles[x][y] = color;
                            else
                                tiles[x][y] = -1;

                            objectsToBeDrawn.AddLast(new DrawnInstance(tileTemplates[color].texture, tileWidth * x, tileHeight * y, tileWidth, tileHeight));
                            break;

                    }
                }
            }


        }

    }
}
