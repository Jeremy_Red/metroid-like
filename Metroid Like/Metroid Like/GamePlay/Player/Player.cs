﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metroid_Like.Graphics;
using Metroid_Like.GamePlay.Physics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
namespace Metroid_Like.GamePlay.Player
{
    class Player : AnimatedInstance
    {
        private BoundingBox collisionBox;

        public Player(ContentManager manager,string texture,int spawnX,int spawnY)
        {
            //load the texture
            Texture = manager.Load<Texture2D>(texture);
            X = spawnX;
            Y = spawnY;
            Width = 32;
            Height = 32;
        }
        
        public void HandleInput()
        {
            //handle the input

            //get the keyboard state
            KeyboardState keyStates = Keyboard.GetState();
            if (keyStates.IsKeyDown(Keys.A)) X -= 5;
            if (keyStates.IsKeyDown(Keys.D)) X += 5;
            if (keyStates.IsKeyDown(Keys.W)) Y -= 5;
            if (keyStates.IsKeyDown(Keys.S)) Y += 5;
        }

        public BoundingBox GetBoundingBox()
        {
            return collisionBox;
        }
    }
}
