﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Metroid_Like.GamePlay.Physics
{
    class BoundingBox
    {
        //the x coord of the BoundingBox
        private float x;
        //the y coord of the BoundingBox
        private float y;
        //the halfWidth of the BoundingBox
        private float halfWidth;
        //the halfHeight of the BoundingBox
        private float halfHeight;

        //the radius is used for optimization purposes only for a broadphase collision check
        private float radius;


        public float CX
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
                //recalulate optimization radius
                calculateOptimizationRadius();
            }
        }

        public float CY
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
                //recalulate optimization radius
                calculateOptimizationRadius();
            }
        }

        public float HalfWidth
        {
            get
            {
                return halfWidth;
            }
            set
            {
                halfWidth = value;
                //recalulate optimization radius
                calculateOptimizationRadius();
            }
        }

        public float HalfHeight
        {
            get
            {
                return halfHeight;
            }
            set
            {
                halfHeight = value;
                //recalulate optimization radius
                calculateOptimizationRadius();
            }
        }

        public BoundingBox(float x = 0,float y = 0,float halfWidth = 0,float halfHeight = 0)
        {
            this.x = x;
            this.y = y;
            this.halfWidth = halfWidth;
            this.halfHeight = halfHeight;

            //calcualte the radius distance
            calculateOptimizationRadius();
        }

        private void calculateOptimizationRadius()
        {
            //calculate the radius
            radius =(float) Math.Sqrt(Math.Pow(x + halfWidth, 2) + Math.Pow(y + halfHeight, 2));
        }

        public void collideAndCorrect(BoundingBox other)
        {
            Vector2 correction = CalculateCorrectionVector(other);
            //apply the correction vector
            x += correction.X;
            y += correction.Y;

        }

        public Vector2 CalculateCorrectionVector(BoundingBox other)
        {
            Vector2 correctionVector = Vector2.Zero;
            //check to see if its even possible for them to collide
            if (Math.Pow(x - other.x, 2) + Math.Pow(y - other.y, 2) > Math.Pow(radius + other.radius, 2))
                return Vector2.Zero;
            float leftOverlap;
            float rightOverlap;
            float topOverlap;
            float bottomOverlap;
            leftOverlap = x - halfWidth - (other.x + other.halfWidth);
            rightOverlap = x + halfWidth - (other.x - other.halfWidth);
            topOverlap = y + halfHeight - (other.y - other.halfHeight);
            bottomOverlap = y - halfHeight - (other.y + other.halfHeight);

            //calculate the appropriate place
            if(bottomOverlap > 0 && leftOverlap > 0|| rightOverlap > 0)
            {
                correctionVector.Y = bottomOverlap;
            }
            else if (topOverlap > 0 && leftOverlap > 0 || rightOverlap > 0)
            {
                correctionVector.Y = -topOverlap;
            }

            if (leftOverlap > 0 && topOverlap> 0 || bottomOverlap > 0)
            {
                correctionVector.X = leftOverlap;
            }
            else if (rightOverlap > 0 && topOverlap > 0 || bottomOverlap > 0)
            {
                correctionVector.X = -rightOverlap;
            }

            return correctionVector;
        }
    }
}
